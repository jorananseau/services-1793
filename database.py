import psycopg2
import datetime
import json

with open('config.json', 'r') as f:
    config = json.load(f)

currentOrganisation = None
dataMediaObject = []
cur = None

def connectDB():
    try:
        global conn
        global cur
        print("Connecting to database...")
        conn = psycopg2.connect(dbname = config['database']['dbname'], user = config['database']['user'], password = config['database']['password'], host = config['database']['host'], port = config['database']['port'])
        cur = conn.cursor()
        print("Connected!")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        exit()

def endDBCon():
    global cur
    if cur is None:
        exit()
    try:
        print("Disconnecting form database...")
        cur.close()
        conn.close()
        print("Disconnected!")
    except Exception as e:
        print(e)
   
def getDataFromMediaobject():
    global cur
    global dataMediaObject
    global currentOrganisation
    print("Executing query...")
    cur.execute(currentOrganisation['query'])
    print("Fetching data...")
    for data in cur.fetchall():
        print(data)
        dataMediaObject.append(data)
    print("Data fetched!")
    print("There are " + str(len(dataMediaObject)) + " files that have the status \'in_progress\'") 
    


def resetVariables():
    global dataMediaObject
    dataMediaObject.clear()