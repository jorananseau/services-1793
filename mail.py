import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json
with open('config.json', 'r') as f:
    config = json.load(f)

counter = None
mailBody = None
mailTo = None
subject = None
message = None

def createMail():
    global mailBody
    mailBody = MIMEMultipart()   
    mailBody['From']= config['sendEmail']['email']
    mailBody['To']= ', '.join(mailTo)
    mailBody['Subject']=subject
    mailBody.attach(MIMEText(message, 'plain'))

def sendMail():
    global mailBody
    try:
        context = ssl.create_default_context()
        with smtplib.SMTP(config['sendEmail']['smtp_server'], config['sendEmail']['smtp_port']) as server:
            print("Connecting to SMTP server...")
            connection = smtplib.SMTP(host=config['sendEmail']['smtp_server'], port=config['sendEmail']['smtp_port'])
            connection.starttls()
            connection.login(config['sendEmail']['email'], config['sendEmail']['password'])
            print("Sending mail...")
            connection.send_message(mailBody)
            print("Mail(s) has been sent!")
    except Exception as e:
        print(e)
    finally:
        connection.quit()
        print("Disconnecting from SMTP server")

    
