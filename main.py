import database
import mail
import json

with open('config.json', 'r') as f:
    config = json.load(f)

def addDataToMail(object):
    mail.mailTo = object['email']
    mail.subject = object['mailSubject']
    mail.message = object['message']

try:
    database.connectDB()
    for object in config['objects']:
        database.currentOrganisation = object
        database.getDataFromMediaobject()
        addDataToMail(object)
        limit = 0
        if "inProgressLimit" in object:
             limit = object['inProgressLimit']
        if (len(database.dataMediaObject) > limit):
            mail.createMail()
            mail.sendMail()
        database.resetVariables()
except Exception as e:
    print(e)
finally:
    database.endDBCon()

