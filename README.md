# Notify Script

## Installation

Clone the repository in the desired folder
```sh
git clone git@bitbucket.org:jorananseau/services-1793.git
```
## Usage
1. Open the config.json file
2. At "database" enter the dbname, hostname, login and password. 
---
**NOTE**

The hostname can be an IP adress or an FQDN

---

3.  At "sendMail" enter the email, password, smtp-server, smtp-port.
---
**NOTE**

* If you use 2FA authentication try to create an application password. This way you can bypass the 2FA.
* To find the smtp-server of your provider search on google for "smtp-server \<your provider>"

---

4. For each organisation you fill in the template (see below)
---
**NOTE**

* If you want to send mails to multiple email addresses put them in aan array as show below ex: 
```js
"email": ["user@gmail.com", "user@gmail.com"],
```
* At inProgressLimit you need to fill in the number of files that need to be in progress before a mail will be sent. The attribute "inProgressLimit" is not obligated. If it is not mentioned in the template, the default wil be taken wich is 0. 
---
**Template**
```js
{
    "query": "your query",
    "inProgressLimit": 0,
    "email": ["your email"],
    "mailSubject" : "Your mail subject",
    "message": "Your mail message"
},
```
        
